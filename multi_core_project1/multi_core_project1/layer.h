#include <stdio.h>
#pragma once
class matrix
{
	int rows;
	int cols;
	char* name;
public:
	double** mat;
	matrix(int r, int c, char* n) {
		rows = r;
		cols = c;
		name = n;
		mat = alloc2Ddouble();
		init();
		printf("%s 매트릭스 생성\n", name);
	}
	~matrix() {
		printf("%s 매트릭스 소멸\n", name);
		clear();
	}
	void clear() {
		free2Ddouble();
		delete name;
	}

	void init() {
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < cols; j++)
				mat[i][j] = 0;
	}
	void realloc(matrix y) {
		free2Ddouble();
		rows = y.row();
		cols = y.col();
		mat = alloc2Ddouble();
		mat = y.mat;
	}

	int row() { return rows; }
	int col() { return cols; }
	
	double** alloc2Ddouble() {
		if (rows <= 0 || cols <= 0) return NULL;



		double** mat = new double* [rows];
		for (int i = 0; i < rows; i++)
			mat[i] = new double[cols];
		return mat;
	}

	void free2Ddouble() {
		if (mat != NULL) {
			for (int i = 0; i < rows; i++)
				delete[] mat[i];
			delete mat;
		}
	}

	matrix* product(matrix* b) {
		matrix* prod = new matrix(this->row(), b->col(), "prod");
		for (int i = 0; i < this->row(); i++) {
			for (int j = 0; j < this->col(); j++) {
				for (int k = 0; k < b->col(); k++) {
					prod->mat[i][k] += (this->mat[i][j] * b->mat[j][k]);
					
				}
			}
		}
		
		return prod;
	}

	void print() {
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				printf("%6.3lf ", mat[i][j]);
			}
			printf("\n");
		}
		printf("\n");
	}
};

class Relu
{
	matrix* mask;
public:
	Relu() {
		mask = NULL;
	}
	~Relu() {
		delete mask;
	}
	matrix* foward(matrix* x) {
		mask = new matrix(x->row(), x->col(), "mask");
		matrix* out = new matrix(x->row(), x->col(), "relu_out");
		
		for (int i = 0; i < x->row(); i++) {
			for (int j = 0; j < x->col(); j++) {
				if (x->mat[i][j] <= 0) {
					mask->mat[i][j] = 0;
					out->mat[i][j] = 0;
				}
				else {
					mask->mat[i][j] = 1;
					out->mat[i][j] = x->mat[i][j];
				}
			}
		}
		
		return out;
	}
	/*
	matrix foward(matrix x) {
		mask = new matrix(x.row(), x.col());
		matrix out(x.row(), x.col());
		for (int i = 0; i < x.row(); i++) {
			for (int j = 0; j < x.col(); j++) {
				if (x.mat[i][j] <= 0) {
					mask->mat[i][j] = 0;
					out.mat[i][j] = 0;
				}
				else {
					mask->mat[i][j] = 1;
					out.mat[i][j] = x.mat[i][j];
				}
			}
		}
		return out;
	}
	*/

	void backward(matrix dout) {
		for (int i = 0; i < dout.row(); i++) {
			for (int j = 0; j < dout.col(); j++) {
				if (mask->mat[i][j] == 0) dout.mat[i][j] = 0;
			}
		}
	}
};
 

class Affine
{
	matrix* weights;
	matrix* bias;

	matrix* features;

	matrix* dweights;
	matrix* dbias;
public:
	Affine(matrix* w, matrix* b) {
		weights = w;
		bias = b;
		features = NULL;
		dweights = NULL;
		dbias = NULL;
	}

	~Affine() {

	}
	
	matrix* forward(matrix* x) {
		features = x;
		matrix* out = features->product(weights);
		for (int i = 0; i < out->row(); i++)
			for (int j = 0; j < out->col(); j++)
				out->mat[i][j] += bias->mat[0][j];
		return out;
	}
};

class SoftmaxWithLoss
{
	/*
	double loss
	double[] y or matrix[] y // softmax 출력
	double[] t //정답 레이블
	*/
public:
	SoftmaxWithLoss() {

	}

	~SoftmaxWithLoss() {

	}

	void forward() {

		
		/*
		t = t
		y = y
		loss = 크로스엔트로피(y, t)
		*/
	}

	void backward() {

		/*
		dx = y - 1 / batch
		return dx
		*/
	}
};
