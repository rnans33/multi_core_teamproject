#include <stdio.h>
#include <cstdlib>
#include <vector>
#include <ctime>
using namespace std;
#include "layer.h"
double random_double_range(double a, double b);
void setrandouble(matrix* x);
int main()
{
	srand((unsigned int)time(0));

	matrix* x = new matrix(10, 20, "x");

	matrix* w = new matrix(20, 10, "w");
	matrix* b = new matrix(1, 10, "b");
	matrix* w2 = new matrix(10, 5, "w2");
	matrix* b2 = new matrix(1, 5, "b2");

	matrix* y;
	setrandouble(x);
	setrandouble(w);
	setrandouble(b);
	setrandouble(w2);
	setrandouble(b2);

	Affine* A1 = new Affine(w, b);
	Affine* A2 = new Affine(w2, b2);
	Relu* relu1 = new Relu();
	Relu* relu2 = new Relu();

	y = A1->forward(x);
	y = relu1->foward(y);
	y->print();
	y = A2->forward(y);
	y = relu2->foward(y);
	y->print();
	
	/*y = relu.foward(x);
	
	for (int i = 0; i < 10; i++) {
		for (int j = 0; j < 20; j++) {
			printf("%lf ", y.mat[i][j]);
		}
		printf("\n");
	}
	
	*/

	return 0;
}

void setrandouble(matrix* x) {
	for(int i = 0; i < x->row(); i++) 
		for(int j =0; j < x->col(); j++)
			x->mat[i][j] = random_double_range(-2, 2);
}

double random_double_range(double a, double b) {
	return ((b - a) * ((double)rand() / RAND_MAX)) + a;
}

void forward(Affine* a, matrix* x) {
	a->forward(x);
}

void learning()
{
	// 데이터 읽기

	// 하이퍼 파라매터 설정

	/*
	for epoch:
		x_batch, t_batch = 데이터 로더

		grad = 기울기 계산

		매개변수 갱신

		loss


	*/
}
